import { Telefone } from './../src/Telefone';

let formulario = <HTMLFormElement> document.querySelector('form');
let campo = <HTMLInputElement> document.querySelector('input');
let impressora = <HTMLElement> document.querySelector('.resultado');

formulario.addEventListener('submit', function(Evento){
    Evento.preventDefault();

    let numeroMascarado = (new Telefone).mascarar(campo.value);

    impressora.textContent = numeroMascarado;
});
