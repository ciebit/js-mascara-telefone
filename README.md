# Máscara de Telefone

Módulo para aplicação de máscara a telefones com 10 e 11 digitos.

## Compilar

Para criar um arquivo Javascript utilizando o padrão de módulos AMD.

```
npm run-script compilar-amd
```
